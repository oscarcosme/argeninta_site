<?php include 'fixed/header.php'; ?>
<?php include 'fixed/menu.php'; ?>

<section class="page asistencia">
	<div class="breadcrumbCustom">
		<div class="container">
			<a href="">Home</a> / <a href="">Asistencia tecnica y economica</a> / Apoyo tecnico y economico
		</div>
	</div>
	<div class="content">
		<div class="bgWhite"></div>
		<div class="container">
			<div class="row">
				<div class="col-12"><h1>Apoyo técnico y económico a iniciativas de desarrollo</h1></div>
				<div class="col-md-6">
					<p>Queremos que nuestro emblema sea el del trabajo asociado a la producción, al agregado de valor, a los saberes, a la unión y a las tradiciones.</p>
					<p>Junto a su capacidad para gestionar proyectos productivos con profundo alcance social, la Fundación apoya técnica y económicamente a quienes  implementan en el territorio iniciativas productivas con componentes tecnológicos e impacto social.</p>
					<p>En la Fundación ArgenINTA se promueve el desarrollo humano sustentable. La solidaridad es la base para el desarrollo por esto es que aportamos nuestros recursos para mejorar la calidad de vida de todos los argentinos.</p>
					<p>Contamos con esta herramienta de financiación, PROGRAMA INTERRIS, Innovación Territorial Sustentable,  para fortalecer el desarrollo, robustecer las economías regionales y locales desde el sector agropecuario y agroindustrial brindando apoyo económico y técnico a emprendimientos con potencialidad para mejorar las condiciones de vida en el territorio y que cuentan con problemas para el acceso al crédito en el sistema financiero.</p>
				</div>
				<div class="col-md-6 d-flex align-items-center">
					<div class="thumbnail">
						<img class="img-fluid" src="assets/img/thumbnail.png" alt="" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content bgNone">
		<div class="container">
			<div class="row py-5 tarjetas">
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							¿En qué consiste la Asistencia Financiera?
						</div>
						<div class="card-body">
							<ul>
								<li>En el otorgamiento de Créditos Reembolsables a baja tasa de interés y en plazos que se ajustan al ciclo productivo-comercial  de la actividad descripta en el proyecto presentado.</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card">
						<div class="card-header">
							¿Quiénes pueden solicitarlo?
						</div>
						<div class="card-body">
							<ul>
								<li>Cooperativas Agropecuarias u otras formas jurídicas sin fines de lucro que tengan relación directa y estén avaladas por el INTA.</li>
								<li>Grupos de Productores, conformados por un mínimo de cinco (5) sin pertenecer a la misma unidad familiar, bajo formas asociativas (jurídicamente constituidas o no) que tengan relación directa y estén avaladas por el INTA. En el caso de las regiones extra-pampeanas el número mínimo de integrantes será de tres (3).</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div id="accordion" class="accordionCustom">
				<div class="card">
					<div class="card-header" id="headingOne">
						<h5 class="mb-0">
							<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
								<span><img src="assets/img/icon-edificio.png" alt="" /></span>
								Destino del Financiamiento
							</button>
							<i class="fas fa-caret-right"></i>
						</h5>
					</div>

					<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
						<div class="card-body">
							<ul>
								<li>Inversión.</li>
								<li>Capital de Trabajo.</li>
								<li>Comercialización: prefinanciación de ventas; almacenamiento y retención de productos, mejoras de packaging, inscripción de productos, códigos de barra.</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingTwo">
						<h5 class="mb-0">
							<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
								<span><img src="assets/img/icon-dolar.png" alt="" /></span>
								Monto a Financiar
							</button>
							<i class="fas fa-caret-right"></i>
						</h5>
					</div>
					<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
						<div class="card-body">
						<ul>
							<li>El monto máximo a financiar por proyecto es de $ 700.000, sin exceder los $100.000 por prestatario</li>
						</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingThree">
						<h5 class="mb-0">
							<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
								<span><img src="assets/img/icon-repago.png" alt="" /></span>
								Plazo de Repago
							</button>
							<i class="fas fa-caret-right"></i>
						</h5>
					</div>
					<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
						<div class="card-body">
							<ul>
								<li>Se fijará atendiendo al ciclo productivo-comercial del proyecto que se trate. El plazo máximo para la devolución del crédito será de tres (3) años, incluyendo el año de gracia.</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header" id="headingFour">
						<h5 class="mb-0">
							<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
								<span><img src="assets/img/icon-interes.png" alt="" /></span>
								Tasa de interés
							</button>
							<i class="fas fa-caret-right"></i>
						</h5>
					</div>
					<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
						<div class="card-body">
							<ul>
								<li>Será del cincuenta por ciento (50 %) de la Tasa Nominal Anual Vencida con Capitalización cada 30 días del Banco de la Nación Argentina vigente al momento de ser aprobado el proyecto por el Consejo de Administración de la Fundación. La tasa resultante se mantendrá fija durante el plazo total de amortización.</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<h2 class="py-2">Criterios de Elegibilidad para el Apoyo Financiero.</h2>
			<p>Para el otorgamiento de la asistencia financiera, se tendrá especialmente en cuenta a aquellos proyectos que:</p>
			<ul class="pb-5">
				<li>Generen recursos genuinos y creen empleo sustentable.</li>
				<li>Conlleven el uso de tecnologías apropiables para la comunidad del territorio donde se desarrolla.</li>
				<li>Involucren a estratos de productores o instituciones con dificultades para el acceso al crédito en el sistema bancario tradicional.</li>
				<li>Tengan la potencialidad de mejorar la calidad de vida de la población rural de los territorios en donde se desarrollen, logrando sostenibilidad social y ambiental.</li>
				<li>Se priorizarán aquellos proyectos que involucren la mayor cantidad de beneficiarios</li>
				<li>Para el destino exclusivo comercialización, se priorizarán proyectos que demuestren una mejora sustancial en el proceso comercial.</li>
			</ul>
		</div>
	</div>
</section>


<?php include 'fixed/footer.php'; ?>
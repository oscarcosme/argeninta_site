<?php include 'fixed/header.php'; ?>
<?php include 'fixed/menu.php'; ?>

<section class="page internacional">
	<div class="breadcrumbCustom">
		<div class="container">
			<a href="">Home</a> / <a href="">Area Internacional</a> / Entidad gestora, Unidad ejecutiva
		</div>
	</div>
	<div class="container">
		<div id="accordion" class="accordionCustom">
			<div class="card">
				<div class="card-header" id="headingOne">
					<h5 class="mb-0">
						<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							<span><img src="assets/img/icon-entidad.png" alt="" /></span>
							Entidad gestora / Unidad ejecutora
						</button>
						<i class="fas fa-caret-right"></i>
					</h5>
				</div>

				<div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
					<div class="card-body">
						<p>Esta modalidad incluye la gestión administrativa-contable y financiera, como así también la gestión operativa y técnica de los proyectos, brindando una herramienta integrada e integral para la ejecución de los mismos.  Permitirá llevar a cabo la coordinación general del proyecto, es decir, que comprende no solo la tarea administrativa-financiera sino que también todas las actividades que  demande el desarrollo del proyecto. Incluye la realización de todas las actividades necesarias que conciernen a la consecución del proyecto.</p>

						<h4>Dichas labores incluirán:</h4>
						<ul>
							<li>Gestión contable (preparación de informes, rendiciones, etc.) según normas del Donante.</li>
							<li>Compras y contratación de servicios, de acuerdo a las normas y procedimientos de  los organismos donantes articulados con los del beneficiario y los de la Fundación (licitaciones, concursos, etc.).</li>
							<li>Organización de eventos/capacitaciones (pasajes, viáticos, alojamiento, coordinación, etc.).</li>
							<li>Informes técnicos en base a la evolución del proyecto</li>
						</ul>
						<p>A través de la experiencia con organizaciones multilaterales/internacionales de préstamo/ donación la Fundación ArgenINTA cuenta con manuales de operaciones de proyectos para BID y Unión Europea.</p>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingTwo">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							<span><img src="assets/img/icon-user.png" alt="" /></span>
							Asistencia Técnica
						</button>
						<i class="fas fa-caret-right"></i>
					</h5>
				</div>
				<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
					<div class="card-body">
						<p>Consiste en la provisión de servicios técnicos y profesionales especializados para el desarrollo, preparación, evaluación o monitoreo (total o parcial) de proyectos o convenios, ya sea en forma exclusiva o asociados con otros organismos o empresas públicas o privadas.</p>
						<p>A los efectos de la provisión de estos servicios, ya sea en forma individual o asociados con otras organizaciones, de acuerdo a la necesidad del proyecto, se cuenta con información referida a expertos y/o consultores propios y asociados (personas jurídicas del sector público y privado), así como terceros (personas físicas). La propuesta de expertos o consultores a las organizaciones interesadas en este servicio se realizará siempre con previo consentimiento explícito del profesional identificado.</p>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingThree">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
							<span><img src="assets/img/icon-book.png" alt="" /></span>
							Administración de proyectos
						</button>
						<i class="fas fa-caret-right"></i>
					</h5>
				</div>
				<div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
					<div class="card-body">
						<p>Incluye la administración financiera-contable de los fondos y la gestión administrativa derivada de las obligaciones emanadas del convenio/carta acuerdo.</p>
						<p>La administración de fondos será llevada a cabo a través de la estructura orgánica institucional vigente.</p>
						<p>Para cada uno de estos universos se llevarán registros contables independientes, atendiendo a las características y condiciones del donante y los procedimientos institucionales certificados bajo norma ISO 9001-2008.</p>
						<p>A través de la experiencia con organizaciones multilaterales/internacionales de préstamo/ donación la Fundación ArgenINTA cuenta con manuales de operaciones de proyectos para BID y Unión Europea</p>
					</div>
				</div>
			</div>
			<div class="card">
				<div class="card-header" id="headingFour">
					<h5 class="mb-0">
						<button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
							<span><img src="assets/img/icon-news.png" alt="" /></span>
							Boletin electrónico
						</button>
						<i class="fas fa-caret-right"></i>
					</h5>
				</div>
				<div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
					<div class="card-body">
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'fixed/footer.php'; ?>
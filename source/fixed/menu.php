<div id="idDivContHeader"></div>
<header>
	<div class="container">
		<div class="row d-flex align-items-center">
			<div class="col-md-12 col-lg-4 col-sm-12">
				<div class="row">
					<div class="col">
						<a class="logo" href="index.php">
							<img src="assets/img/logo.jpg" alt="" />
						</a>
					</div>
					<div class="col text-right">
						<div class="iconNav">
							<i class="fas fa-bars"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12 col-lg-8 col-sm-12 breakTop">
				<div class="top-one">
					<div class="redes">
						<ul>
							<li>Siguenos en:</li>
							<li>
								<a href="">
									<i class="fab fa-facebook-f"></i>
								</a>
							</li>
							<li>
								<a href="">
									<i class="fab fa-twitter"></i>
								</a>
							</li>
							<li>
								<a href="">
									<i class="fab fa-instagram"></i>
								</a>
							</li>
						</ul>
					</div>
					<div class="search">
						<input type="text" placeholder="Buscar">
						<i class="fas fa-search"></i>
					</div>
					<!-- <div class="email">
						<a href="">info@argeninta.org</a>
					</div> -->
					<div class="phone">
						<ul>
							<li><i class="fas fa-phone-volume"></i></li>
							<li>011 4802-6101</li>
						</ul>
					</div>
				</div>
				<div class="top-two">
					<!-- <ul>
						<li><a href="institucional.php">Institucional</a></li>
						<li><a href="#">Prensa</a></li>
						<li><a href="#">Trámites</a></li>
						<li><a href="#">Contacto</a></li>
						<li><a href="#"><i class="fas fa-user"></i> Intranet</a></li>
					</ul> -->
					<nav class="navbar navbar-expand-lg navbar-light submenu">
						<div class="navbar-collapse" id="navbarNavDropdownOne">
							<ul class="navbar-nav">
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Institucional
									</a>
									<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
										<a class="dropdown-item" href="institucional.php">¿Qué es argenINTA?</a>
										<a class="dropdown-item" href="#">Estructura</a>
										<a class="dropdown-item" href="#">Delegaciones</a>
										<a class="dropdown-item" href="#">Recursos humanos</a>
									</div>
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Prensa
									</a>
									<!-- <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
										<a class="dropdown-item" href="#">Action</a>
									</div> -->
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Trámites
									</a>
									<!-- <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
										<a class="dropdown-item" href="#">Action</a>
									</div> -->
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										Contacto
									</a>
									<!-- <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
										<a class="dropdown-item" href="#">Action</a>
									</div> -->
								</li>
								<li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
										<i class="fas fa-user"></i> Intranet</a>
									</a>
								</li>
							</ul>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>

<nav class="navbar navbar-expand-lg navbar-light forDesktop">
	<div class="container">
		<div class="navbar-collapse" id="navbarNavDropdownTwo">
			<ul class="navbar-nav">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="asistencia.php" id="navbarDropdownMenuLink" data-toggle="" aria-haspopup="true" aria-expanded="false">
						Asistencia técnica y económica
					</a>
					<!-- <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="#">¿Qué es argenINTA?</a>
						<a class="dropdown-item" href="#">Estructura</a>
						<a class="dropdown-item" href="#">Delegaciones</a>
						<a class="dropdown-item" href="#">Recursos humanos</a>
					</div> -->
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Capacitación
					</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Del territorio al plato
					</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Comercialización
					</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="area-internacional.php" id="navbarDropdownMenuLink" data-toggle="" aria-haspopup="true" aria-expanded="false">
						Área internacional
					</a>
				</li>
			</ul>
		</div>
	</div>
</nav>
		


<div class="navMobile">
	<a class="logo" href="#">
		<img src="assets/img/logo.jpg" alt="" />
	</a>
	<nav class="navbar navbar-expand-lg navbar-light forMobile">
		<div class="navbar-collapse" id="navbarNavDropdown">
			<ul class="navbar-nav">
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Asistencia técnica y económica
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="#">Financiamiento</a>
						<a class="dropdown-item" href="#">Apoyo técnico y financinanciero interris</a>
						<a class="dropdown-item" href="#">El programa</a>
						<a class="dropdown-item" href="#">Destinatarios</a>
						<a class="dropdown-item" href="#">Inversión</a>
						<a class="dropdown-item" href="#">Montoz</a>
						<a class="dropdown-item" href="#">Montoz</a>
						<a class="dropdown-item" href="#">Elegibilidad</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Capacitación
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="#">Online</a>
						<a class="dropdown-item" href="#">Presencial</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Del territorio al plato
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="#">Acciones</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Comercialización
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="#">Acceso a mercados</a>
						<a class="dropdown-item" href="#">Mejora y diferenciación de producto</a>
						<a class="dropdown-item" href="#">Ferias</a>
						<a class="dropdown-item" href="#">Destinatarios</a>
						<a class="dropdown-item" href="#">Inversión</a>
						<a class="dropdown-item" href="#">Montoz</a>
						<a class="dropdown-item" href="#">Plazos</a>
						<a class="dropdown-item" href="#">Elegibilidad</a>
					</div>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="area-internacional.php" id="navbarDropdownMenuLink" data-toggle="" aria-haspopup="true" aria-expanded="false">
						Área internacional
					</a>
					<!-- <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
						<a class="dropdown-item" href="#">Action</a>
						<a class="dropdown-item" href="#">Another action</a>
						<a class="dropdown-item" href="#">Something else here</a>
					</div> -->
				</li>
			</ul>
		</div>
	</nav>
</div>


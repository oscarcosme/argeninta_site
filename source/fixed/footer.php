	<div id="footer">
		<div class="container-fluid py-3">
			<div class="row">
				<div class="lista col-md-1 col-sm-4 text-sm-center text-md-left">
					<h5>Institucional <i class="fas fa-sort-down"></i></h5>
					<ul>
						<li><a>Que es argenINTA</a></li>
						<li><a>Estructura</a></li>
						<li><a>Delegaciones</a></li>
						<li><a>Recursos humanos</a></li>
					</ul>
				</div>
				<div class="lista col-md-2 col-sm-4 text-sm-center text-md-left">
					<h5>Asistencia tecnica y eco <i class="fas fa-sort-down"></i></h5>
					<ul>
						<li><a>Financiamiento</a></li>
						<li><a>Apoyo técnico y financinanciero interris</a></li>
						<li><a>El programa</a></li>
						<li><a>Destinatarios</a></li>
						<li><a>Inversión</a></li>
						<li><a>Montoz</a></li>
						<li><a>Montoz</a></li>
						<li><a>Elegibilidad</a></li>
					</ul>
				</div>
				<div class="lista col-md-1 col-sm-4 text-sm-center text-md-left">
					<h5>Capacitación <i class="fas fa-sort-down"></i></h5>
					<ul>
						<li><a>Online</a></li>
						<li><a>Presencial</a></li>
					</ul>
				</div>
				<div class="lista col-md-2 col-sm-4 text-sm-center text-md-left">
					<h5>Del territorio del plato <i class="fas fa-sort-down"></i></h5>
					<ul>
						<li><a>Acciones</a></li>
					</ul>
				</div>
				<div class="lista col-md-2 col-sm-4 text-sm-center text-md-left">
					<h5>Comercializacion <i class="fas fa-sort-down"></i></h5>
					<ul>
						<li><a>Acceso a mercados</a></li>
						<li><a>Mejora y diferenciación de producto</a></li>
						<li><a>Ferias</a></li>
						<li><a>Destinatarios</a></li>
						<li><a>Inversión</a></li>
						<li><a>Montoz</a></li>
						<li><a>Plazos</a></li>
						<li><a>Elegibilidad</a></li>
					</ul>
				</div>
				<div class="lista col-md-1 col-sm-4 text-sm-center text-md-left">
					<h5>Internacional <i class="fas fa-sort-down"></i></h5>
				</div>
				<div class="lista col-md-1 col-sm-4 text-sm-center text-md-left">
					<h5>Prensa <i class="fas fa-sort-down"></i></h5>
					<ul>
						<li><a>Acceso a mercados</a></li>
						<li><a>Mejora y diferenciación de productos</a></li>
						<li><a>Ferias</a></li>
					</ul>
				</div>
				<div class="lista col-md-1 col-sm-4 text-sm-center text-md-left">
					<h5>Tramites <i class="fas fa-sort-down"></i></h5>
					<ul>
						<li><a>Los servicios</a></li>
						<li><a>Oportunidades</a></li>
					</ul>
				</div>
				<div class="lista col-md-1 col-sm-4 text-sm-center text-md-left">
					<h5>Contactos <i class="fas fa-sort-down"></i></h5>
					<!-- <ul>
						<li>-</li>
					</ul> -->
				</div>
			</div>
		</div>
		<div class="container-fluid contacto">
			<!-- <h5>Contacto</h5> -->
			<div><i class="fas fa-map-marker-alt"></i> Dirección: Av. Cerviño 3101, C1425AGA CABA</div>
			<div><i class="far fa-envelope"></i> <a href="mailto:info@argeninta.org">info@argeninta.org</a></div>
			<div><i class="fas fa-phone-volume"></i> <a href="tel:011-4802-610">Teléfono: 011 4802-610</a></div>
			<div class="redes">
				<ul>
					<li>Siguenos en:</li>
					<li>
						<a href="">
							<i class="fab fa-facebook-f"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-twitter"></i>
						</a>
					</li>
					<li>
						<a href="">
							<i class="fab fa-instagram"></i>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="container-fluid copy">
			<p>&copy; Copyright 2018 Todos los derechos reservados.</p>
		</div>
	</div>

	<!-- LIGHTBOX -->
		

	<script type="text/javascript" charset="utf-8" src="libs/jquery/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="libs/modernizr/modernizr.js"></script>
	<script type="text/javascript" charset="utf-8" src="libs/flowtype/flowtype.js"></script>
	<script type="text/javascript" charset="utf-8" src="libs/slick/slick.min.js"></script>
	<script type="text/javascript" charset="utf-8" src="libs/tweenmax/tweenmax.min.js"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	
	<script type="text/javascript" charset="utf-8" src="assets/js/script.min.js<?php echo $rand; ?>"></script>
	<script type="text/javascript"></script>
		
  </body>
</html>
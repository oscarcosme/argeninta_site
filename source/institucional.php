<?php include 'fixed/header.php'; ?>
<?php include 'fixed/menu.php'; ?>

<section class="page institucional">
	<div class="breadcrumbCustom">
		<div class="container">
			<a href="">Home</a> / <a href="">Institucional</a> / ¿Qué es Argentina?
		</div>
	</div>
	<div class="content">
		<div class="bgWhite"></div>
		<div class="container">
			<ul class="nav nav-pills mb-3 d-flex justify-content-center py-4" id="pills-tab" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">¿Qué es Argentina?</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Estructura</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Delegaciones</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" id="pills-human-tab" data-toggle="pill" href="#pills-human" role="tab" aria-controls="pills-human" aria-selected="false">Recursos humanos</a>
				</li>
			</ul>
			<div class="tab-content py-4" id="pills-tabContent">
				<div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
					<div class="row">
						<div class="col-md-6">
							<!-- <h3>Fundación ArgenINTA junto a RedCOM</h3> -->
							<p>Creada por el Instituto Nacional de Tecnología Agropecuaria en 1993, la Fundación ArgenINTA, es una institución sin fines de lucro que cuenta con la tarea de facilitar la consecución de los objetivos de dicho Instituto y de promover el desarrollo sustentable autónomo con un enfoque regional y territorial dentro de una visión nacional.</p>
							<p>Conformando un espacio institucional que facilita la relación entre lo público y lo privado, ArgenINTA está regida por un Consejo de Administración integrado por el INTA, Confederaciones Rurales Argentinas (CRA),  Asociación Argentina de Consorcios Regionales de Experimentación Agrícola (AACREA), Federación Agraria Argentina (FAA),  Sociedad Rural Argentina (SRA), Confederación Intercooperativa Agropecuaria (CONINAGRO), Ministerio de Agroindustria de la Nación, Facultades de Agronomía, Facultades de Veterinaria, dos miembros benefactores provenientes de una entidad o empresa agroindustrial y un representante de los Consejos de Centros Regionales de INTA.</p>
						</div>
						<div class="col-md-6">
							<div class="thumbnail">
								<img class="img-fluid" src="assets/img/thumbnail.png" alt="" />
							</div>
						</div>
						<div class="col-md-12 py-5">
							<p>ArgenINTA brinda servicios como Unidad de Vinculación Tecnológica, de consultoría, de asesoramiento, de conducción y administración de proyectos, de búsqueda de oportunidades de financiamiento nacionales e internacionales a diversos organismos públicos nacionales y provinciales. Otorga créditos a cooperativas y grupos de productores organizados y asesorados por el INTA y para iniciativas de emprendedores que contribuyan con el desarrollo, validación y fabricación de tecnologías que permitan la mejora de productos o procesos, con impacto productivo y/o comercial.</p>
							<p>Otorgamos aportes no reembolsables para colaborar con la formalización y fortalecimiento de organizaciones de productores. Microcréditos para la compra de insumos, materia prima, herramientas, reparaciones o mejoras.</p>
							<p>Aportes no reintegrables a distintas instituciones como escuelas, bibliotecas, Capacitaciones presenciales y cursos a distancia vinculados con la calidad y valor agregado.</p>
						</div>
					</div>
				</div>
				<div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis eum eos placeat, id nobis aspernatur, ipsa porro facere saepe dolorem aperiam labore tenetur tempore doloribus exercitationem, doloremque libero delectus vel nisi esse assumenda? Quasi tempore laboriosam a totam adipisci beatae magni eius, debitis voluptatem, molestiae fugiat laudantium quod explicabo maiores?</p>
				</div>
				<div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas ducimus cum doloribus repudiandae, nihil, odit tempora libero eligendi hic vero aperiam, incidunt rerum in porro voluptatum consequuntur veritatis pariatur eos totam quia natus optio suscipit quisquam. Suscipit repudiandae, quisquam impedit in ratione, officia nobis laudantium obcaecati explicabo modi necessitatibus voluptatum, provident veniam voluptatem labore blanditiis ipsum cupiditate doloremque debitis quod.</p>
				</div>
				<div class="tab-pane fade" id="pills-human" role="tabpanel" aria-labelledby="pills-human-tab">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas ducimus cum doloribus repudiandae, nihil, odit tempora libero eligendi hic vero aperiam, incidunt rerum in porro voluptatum consequuntur veritatis pariatur eos totam quia natus optio suscipit quisquam. Suscipit repudiandae, quisquam impedit in ratione, officia nobis laudantium obcaecati explicabo modi necessitatibus voluptatum, provident veniam voluptatem labore blanditiis ipsum cupiditate doloremque debitis quod.</p>
				</div>
			</div>
		</div>
	</div>
</section>

<?php include 'fixed/footer.php'; ?>
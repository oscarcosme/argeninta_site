<?php include 'fixed/header.php'; ?>
<?php include 'fixed/menu.php'; ?>

<div class="boxSlider">
	<div>
		<img src="assets/img/banner.jpg" alt="" />
		<div class="container">
			<div class="sliderExcerpt">
				<div>
					<span>Del campo a la mesa</span>
					<h2><a href="#">El INTA abrió su primer almacén para emprendedores familiares</a></h2>
					<p>Con el apoyo de la Fundación ArgenINTA y de INTEA S. A., el instituto inauguró un espacio para comercializar productos con agregado de valor, elaborados por productores de pequeña y mediana escala que reciben asesoramiento técnico.</p>
					<button class="btn btn-danger moreinfo">+ Info</button>
				</div>
				<div>
					<h2>Siempre sembrando conciencia en los jovenes edición 2018</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat voluptates, sequi dolorem, totam reprehenderit magnam cum fuga! Aut omnis itaque ut, nostrum dolores cupiditate molestias mollitia quis reprehenderit minima</p>
				</div>
				<div>
					<h2>Siempre sembrando conciencia en los jovenes edición 2019</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Placeat voluptates, sequi dolorem, totam reprehenderit magnam cum fuga! Aut omnis itaque ut, nostrum dolores</p>
				</div>
			</div>
		</div>
	</div>
	<div>
		<img src="assets/img/banner.jpg" alt="" />
	</div>
</div>

<article class="vision">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<img src="assets/img/vision.png" alt="" />
			</div>
			<div class="col-md-6 align-self-center boxRight">
				<h2>Nuestra Visión</h2>
				<p>Trabajar territorialmente, entendiendo al territorio no solo como un espacio geográfico si no como un todo que incluye a ese espacio y sobre todo a la comunidad que vive, trabaja y sueña en el.</p>
				<button class="btn btn-danger readmore">Ver más <i class="fas fa-angle-right"></i></button>
			</div>
		</div>
	</div>
</article>

<article class="multimedia">
	<div class="videos">
		<div class="container text-center">
			<h2 class="pb-5">Videos y fotos</h2>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<img src="http://via.placeholder.com/500x300">
						</div>
						<div class="col-md-12 py-4">
							<button class="btn btn-danger">Ver más videos <i class="fas fa-angle-right"></i></button>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<div class="col-md-12">
							<img src="http://via.placeholder.com/500x300">
						</div>
						<div class="col-md-12 py-4">
							<button class="btn btn-danger">Ver más videos <i class="fas fa-angle-right"></i></button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="images">
		<div class="container">
			<div class="row text-center">
				<div class="col-md-4 thumbnail">
					<a href="">
						<img src="http://via.placeholder.com/250x250" alt="" />
					</a>
				</div>
				<div class="col-md-4 thumbnail">
					<a href="">
						<img src="http://via.placeholder.com/250x250" alt="" />
					</a>
				</div>
				<div class="col-md-4 thumbnail">
					<a href="">
						<img src="http://via.placeholder.com/250x250" alt="" />
					</a>
				</div>
			</div>
		</div>
	</div>
</article>

<article class="redes py-5">
	<div class="container">
		<div class="row text-center">
			<div class="col-md-6 text-center">
				Twitter
			</div>
			<div class="col-md-6 text-center">
				Instagram
			</div>
		</div>
	</div>
</article>
























<?php include 'fixed/footer.php'; ?>
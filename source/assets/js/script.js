function overflowHiddenOn() {
	$('html, body').css('overflow', 'hidden');
	//disableScroll();
}
function overflowHiddenOff() {
	$('html, body').css('overflow', 'auto');
	//enableScroll();
}
function esMobile(){
	return ($(window).width()<768)?true:false;
}
function jsScrollTo(elemento){
	$('html, body').animate({
        scrollTop: $(elemento).offset().top
    }, 1000);
}


$(function(){
	g_Preloader.active(true);
	$(document).ajaxStart(function () {
		g_Preloader.active(true);
	}).ajaxStop(function () {
		g_Preloader.active(false);
	});

	$('body').flowtype({
		minimum   : 500,
		maximum   : 1200,
		minFont   : 12,
		maxFont   : 18,
		fontRatio : 40
	});

	$('.boxSlider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		focusOnSelect: true,
		nextArrow: '<button type="button" class="slick-next"><i class="fas fa-arrow-right"></i></button>',
		prevArrow: '<button type="button" class="slick-prev"><i class="fas fa-arrow-left"></i></button>',
		// adaptiveHeight: true,
		responsive: [{
			breakpoint: 415,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1,
				infinite: true
			}
		}]
	});

	$('.sliderExcerpt').slick({
		arrows: false,
		dots: true,
		infinite: true,
		speed: 300,
		slidesToShow: 1,
		autoplay: false
		// adaptiveHeight: true
	});

	$('.boxSlider button.slick-next').addClass('hover');
	$('.boxSlider button').mouseenter(function(){
		$('.boxSlider button').removeClass('hover');
		var puntero = $(this);
		puntero.addClass('hover');
	});

	$('.boxSlider button').mouseleave(function(){
		$('.boxSlider button').removeClass('hover');
		$('.boxSlider button.slick-next').addClass('hover');
	});

	// NAV MOBILE
	$('.iconNav i').on('click', function(){
		$('.navMobile').toggleClass('show');
		if($('.navMobile').hasClass('show')){
			overflowHiddenOn();
		}else {
			overflowHiddenOff();
		}
	});

	// SUBMENU

	// FILTROS - SIDEBAR

	//LLAMADAS AJAX

	// HOVER IMG

	//FOOTER
	$('#footer .lista h5').on('click', function(){
		// console.log('holi');
		$('#footer .lista ul').removeClass('show');
		var puntero = $(this);
		puntero.parent().find('ul').toggleClass('show');
		// setTimeout(function(){
		// 	$('#footer .lista ul').removeClass('show');
		// }, 3000);
	});



	



		







































});



$(window).on('load', function (e) {
	setTimeout(function(){
		g_Preloader.active(false);
	}, 1000);
});

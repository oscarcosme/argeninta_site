function jsSendAjax(strTipo, strUrl, jsonParametros, funcionRetorno){
	$.ajax({
		type: strTipo,
		url: strUrl,
		data: jsonParametros
	}).done(function( data ) {
		jsCallback(data, funcionRetorno);
	});
}
function jsCallback(strRetornoAjax, funcionRetorno){
	funcionRetorno.ejecutar(strRetornoAjax);
}